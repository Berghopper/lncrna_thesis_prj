\section{Gene Ontology}

``The GO consortium is a project that compiles a dynamic, controlled vocabulary of terms related to different aspects of genes and gene products (proteins)".\supercite{binfungen,goconsort}
The GO database is comprised of ``GO terms", these are singular descriptions of concepts which apply to specific genes on different levels.
They are structured like a tree with lessening ambiguity (e.g., more specific further down) and form a Directed Acyclic Graph (DAG) representing the relationships between genes/gene products.
The GO consortium consists of three main organizing terms (with no particular order):

\begin{itemize}
	\item Molecular Function (MF)
	\item Biological Process (BP)
	\item Cellular Component (CC)
\end{itemize}

MF represents gene product function, BP represents the biological pathways or processes the gene product is a part of/is associated with, and CC represents the locus of said product (e.g. ``In the mitochondria").
These three terms are also the root nodes in the DAG structure that are used in the GO database.
To give an example of some GO terms Table \ref{tab:goex} has been compiled below.

\begin{table}[h!]
	\small
	\begin{center}
		\caption{\textit{Table describing some example GO terms, 1 for each major organizing term (ontology). All descriptions and info is based off of the GO consortium.\supercite{goterm} These examples are in no meaningful way related to the project, they only function as examples.}}
		\label{tab:goex}
		\rowcolors{1}{white}{gray!25}
		\begin{tabular}{|l|l|l|} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
			\textbf{GO term ID} & \textbf{Name (short description)} & \textbf{Ontology}\\
			\hline
			GO:0010698 & ``acetyltransferase activator activity" & MF\\
			GO:1905533 &  ``negative regulation of leucine import across plasma membrane" & BP\\
			GO:0010598 & ``NAD(P)H dehydrogenase complex (plastoquinone)" & CC
		\end{tabular}
	\end{center}
\end{table}

To give an idea about the GO term DAG structure, Figure \ref{fig:gotree} is supplied.

\newpage
\begin{figure}[h!]
	\centering
	\includegraphics[height=400px]{images/OR-34-04-1779-g02.jpg}
	\caption{\textit{Cited figure from Zhu et al.. Visualization of the GO term DAG tree and its relationships.\supercite{zhu_go}}}
	\label{fig:gotree}
\end{figure}

To access GO term data, the online repository can be used\supercite{goterm} or can be interfaced via several R packages.\supercite{rproj}
A few examples of such packages include ``GO.db" and all ``OrgDB" packages from Bioconductor.\supercite{BIOC_orgdb,bioc}

\section{Prediction algorithms}

This project used both ``lncRNA2GOA" and ``TopoICSim" as described in the introduction; these algorithms have both been compiled into the GAPGOM package.\supercite{GAPGOM}
The GAPGOM package was created to make a more general interface of using the ``lncRNA2GOA" and ``TopoICSim" algorithms.
Further details about both algorithms are described below.

\subsection*{lncRNA2GOA}

lncRNA2GOA\supercite{lncrnapred}, or lncRNA to GO annotation (prediction), tries to make a prediction on annotation of a novel gene or in our case, an lncRNA.
The scoring is achieved by correlative methods and methods not before used in this type of analysis, mentioned by the paper this algorithm is based on.
The scoring methods include: ``Pearson", ``Spearman", ``Kendall", ``Fisher", and ``Sobolev" of which ``Fisher" and ``Sobolev" are special geometrical measures.

These statistical/scoring methods are used to compare genes/transcripts by their measured expression values.
This can then be used on novel transcripts that have correlated expression with well-annotated genes to create an annotation prediction with a ``guilt by association" assumption.
This algorithm is aimed explicitly towards lncRNAs, but technically, any novel transcript can be analyzed in this way.

Specifically, in lncRNA2GOA, each input gene is compared to the rest of the genes with the described methods.
These scores are then fed to an enrichment algorithm or more specifically to a GO term Enrichment Analysis, that annotates with the most frequent GO terms, sorted by the FDR (Benjamini \& Hochberg adjusted p-value) method.
An enrichment analysis tries to identify over-represented items (in this case GO terms) within a specific set of reference items (genes/transcripts with annotations available).
This enrichment analysis is done by calculating the significant GO terms for correlated protein-coding genes which have an annotation available.
The enrichment analysis is run on each consecutive correlation method, whereof the result with the most significant corrected p-value is kept.

Enrichment analyses are meant to show over-representation in a specific subset of data, providing potentially exciting features of said dataset, in our case GO terms.

\subsection*{TopoICSim}

TopoICSim\supercite{topoicsim} or Topological Information Content Similarity, is a semantic similarity measure.
TopoICSim measures semantic distances between genes, by weighting DAG paths based on the information content (IC) between the respective GO terms in the GO DAG.
Unlike other methods, it considers both the shortest and longest path in the DAG.

The IC calculation is based on \cref{eq:1}.

\begin{equation} \label{eq:1}
IC(t) = -logp(p(t))
\end{equation}

Where $t$ is the respective GO term and $p$ the function to determine the frequency of $t$.\supercite{gosemsim} 

TopoICSim has also been shown to be a more robust method than previous topological algorithms, while still performing reasonably fast.
The method was compared with others, consisting of: ``IntelliGO", ``Wang", ``LordNormalized", ``Al-Mubaid" and ``SimGIC".
The algorithms were compared with the Intraset Discriminative Power (IDP) in the paper and showed that TopoICSim is a robust and reliable measure.\supercite{topoicsim}

The Intraset similarity as in \cref{eq:2}, describes average similarity within a gene set, whilst Interset similarity as in \cref{eq:3} between gene sets.

\begin{equation} \label{eq:2}
IntraSetSim(S_k)=\frac{\sum_{i=1}^n\sum_{j=1}^mSim(g_{ki},g_{kj})}{n^2}
\end{equation}

\begin{equation} \label{eq:3}
InterSetSim(S_k)=\frac{\sum_{i=1}^n\sum_{j=1}^mSim(g_{ki},g_{kj})}{n\times m}
\end{equation}

The discriminative power is defined as a ratio between these two, as it is important to have a high value on the Intraset, whilst also having a big discriminative power between gene sets. The IDP is defined in \cref{eq:5} and based on \cref{eq:4}.

\begin{equation} \label{eq:4}
DP_{Sim}(S_k) = \frac{(p-1){IntraSetSim(S_k)}}{\sum^{p}_{i=1,i\neq k}{InterSetSim(S_k,S_i)}}
\end{equation}

\begin{equation} \label{eq:5}
IDP_{Sim}(S_k) = {IntraSetSim}(S_k)\times DP_{Sim}(S_k)
\end{equation}

``For \cref{eq:2,eq:3,eq:4,eq:5} let $S$ be a collection of genes where $S=\{S_1,S_2,\dots,S_p\}$ (each $S_k$ can be e.g. a Pfam clan or a KEGG pathway). For each $S_k$, let $\{g_{k1},g_{k2},\dots,g_{kn}\}$ be the set of $n$ genes in $S_k$."\supercite{topoicsim}

\section{Principle coordinate  analysis (PCoA)/Classical multidimensional scaling (MDS)}

Principle coordinate analysis (PCoA) or Classical multidimensional scaling (MDS) is an analysis to find spatial coordinates for a $N \times N$ distance matrix while not knowing the original coordinates/data.\supercite{wickel_mds}
This is useful, because TopoICSim similarities are not based on direct coordinates, but rather on information content and the GO DAG tree.
With this calculation, an arbitrary coordinate space may be created, allowing for easy data manipulation such as clustering analyses and visualization.
PCoA or classical MDS is based on Euclidean distances and thus might not apply to problems that describe non-euclidean distances.
However, since classical MDS is the most commonly available algorithm and can still provide a powerful visualization tool, it is assumed TopoICSim distances are Euclidean.

PCoA consists of 4 general steps:

\begin{enumerate}
	\item Setting up a squared proximity matrix $P^{(2)} = [p^2]$ where $p$ is the distance matrix.
	\item Applying double centering: $B = -\frac{1}{2}JP^{(2)}J$, where $B$ is $B = XX'$ and where $J$ is $J = I - n^{-1}11'$  (where $n$ is the number of objects).
	\item Extracting the largest positive eigenvalues $\lambda_1\dots\lambda_m$ of $B$ and the corresponding $m$ eigenvectors $e_1\dots e_m$.
	\item A m-dimensional spatial configuration of $n$ objects that is derived from the coordinate matrix $X = E_{m}\Lambda_{m}^{\frac{1}{2}}$, where $E_m$ is the matrix of $m$  eigenvectors and $\Lambda_m$ the diagonal matrix of $m$ eigenvalues of $B$.
\end{enumerate}

With this analysis, TopoICSim similarities can be transformed into an arbitrary X-dimensional space, which can be visualized.
This can be useful for e.g. determining genes/transcripts involved in similar ontologies.

\section{Random forest classification}

Random forest classification is a machine learning algorithm wherein a multitude of decision trees are merged into a mean decision tree, to further decrease variance than is possible by regular decision trees. This can be done by trying to minimize the amount of correlation between the trees by introducing randomness at certain branch-points.\supercite{randfor}

\section{K-means clustering}

K-means clustering is a clustering algorithm that tries to find $K$ amount of groups in a data set.
More specifically, it is an unsupervised learning algorithm, meant to analyze unlabeled data.
It plots an amount of $K$ points (or centroids) randomly in the data set and iteratively tries to reduce the distance between itself and its surrounding points. This is done until the centroids no longer moved and have reduced the mean to the local optimum for that group. This can be repeated to avoid other, larger local optima.\supercite{kmeans}

\section{Hierarchical clustering}

Like K-means clustering, Hierarchical clustering is an unsupervised learning algorithm. However, instead of choosing a pre-specified $K$ it instead calculates distances between all points and generates a dendrogram of this process. This dendrogram can then also be cut off for a specified value, also creating a $K$ amount of clusters if desired.\supercite{hierclust}