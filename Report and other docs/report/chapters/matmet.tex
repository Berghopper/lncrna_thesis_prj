\section{GO term prediction pipeline}

For both the quality measure benchmark and the lncRNA specific analysis, annotations need to be predicted in large quantities.
This prediction is done with a particular version of the GAPGOM R package\supercite{GAPGOM}, made to be compatible with R version 3.5.x.
This particular version is available in the attachments zip archive.
However, because the R package does not allow for multiprocessing directly, and because different input types of expression sets might be problematic, a pipeline was built.
The pipeline does not only predict annotation, but also calculates TopoICSim scores for several items.
This includes the intraset similarity between predicted GO terms and TopoICSim scores for predicted GO terms versus original GO terms (``re-annotation"/interset similarity).
All genesets and expressions sets are combined in a pairwise manner, for every major GO ontology (MF, BP, CC).
The values of the TopoICSim ``re-annotation" are also combined and plotted per pairwise analysis.

This whole process is also shown as a flowchart in Figure \ref{fig:snekpipe}.

\subsection*{SYMBOL ID conversion}

To make sure the symbol IDs in the genesets and expression sets match properly, SYMBOL IDs are converted to their latest versions using the limma R package.
This might sometimes assign multiple SYMBOLs to e.g. lncRNAs or omit some lncRNAs altogether. For genesets SYMBOL IDs that have no corresponding current ID are discarded.
Specifically, for expression sets, if SYMBOLS overlap the most variant row of expression values is kept.
Translation tables are provided in the ``Supplementary figures and tables" appendix, for both prostate and colorectal cancer in Table \ref{tab:lncrnasym}.
This table was extracted from the TopoICSim cancer-related lncRNA analysis, the translation tables there should be the same across different FDR cutoffs.
This analysis is described later in the methods.

\newpage
\begin{figure}[h!]
	\centering
	\includegraphics[height=500px]{images/flowcharts/snakemake_pipeline.png}
	\caption{\textit{Flowchart of snakemake pipeline describing each of the major steps taken in the process. First expression sets and genesets are prepared to work with the lncRNA2GOA algorithm, as well as GO term annotation data (on the right-middle). lncRNA2GOA then performs annotation predictions on all pairwise combinations of inputs (geneset $\times$ expression set $\times$ ontology). All these annotations are then fed to TopoICSim where both intraset similarity (between predicted annotations) and interset similarity (between predicted annotations and original annotations) is calculated. The latter is combined into several plots per pairwise combination as well as each ontology.}}
	\label{fig:snekpipe}
\end{figure}
\newpage

The pipeline supports a multitude of different settings and inputs, made to be easily tweak-able and adjustable to the analyses.
An example configuration file can be seen below:

\fontsize{10pt}{12}\selectfont
\begin{lstlisting}[frame=single]  % Start your code-block

|\textcolor{red}{\# COMMENT}|
|\textcolor{red}{\# Config file for snakemake pipeline, all directories should have a "/" at the end.}|
|\textcolor{red}{\#\# BENCHMARKS - TOPOICSIM REANNOTATION}|

|\textcolor{red}{\# INPUT FILES - !!! DO NOT USE "-" IN ANY OF YOUR FILES, THIS IS USED}|
|\textcolor{red}{\    AS A WILDCARD SEPERATOR !!!}|
geneset_dir : "./genesets/"
colncrna_expsets : "./expsets_GSE_colncrna/"
morten_geneset_dir : "./genesets_morten/"
morten_expsets : "./expsets_morten/"
prepared_expsets: "./expsets_prepared/"
r_dir : "" |\textcolor{red}{\# ONLY CONFIGURE FOR CUSTOM R BUILDS (may also contain}|
|\textcolor{red}{\    prefixes or prep commands)}|
rscript_dir : "scripts/R_scripts/"
pyscript_dir : "scripts/python3/"
|\textcolor{red}{\# parameters}|
keytype_colncrna : "ENSEMBL"
keytype_morten: "SYMBOL"
variance_morten: "T" |\textcolor{red}{\# whether to calculate expression variance on geneset cutoff}|
variance_colnc: "T"
correct_symbol: "T" |\textcolor{red}{\# correct SYMBOLS with limma? }|
|\textcolor{red}{\    (might remove/add some genes)}|
include_anno_only : "T" |\textcolor{red}{\# whether to only include annotated transcripts for lncRNA2GOA}|
|\textcolor{red}{\    (backwards compatability)}|
geneset_cutoff : "0" |\textcolor{red}{\# 0 = unlimited}|
big_job_cores : "10" |\textcolor{red}{\# recommended to keep on "1" for runs on small machines, for clusters}|
|\textcolor{red}{\    use amount of cores per node (or a fraction of it).}|
lncrna2goa_significance : "0.05"
lncrna2goa_enrichment_cutoff : "250"

|\textcolor{red}{\# OUTPUT CONTROLS}|
main_output_dir : "~/OUTPUTS/"
control_files_dir : "~/OUTPUTS/control_txts/"

\end{lstlisting}
\fontsize{11pt}{12}\selectfont

Configuration variables that are important to the project include the ``geneset\_cutoff", ``lncrna2goa\_significance", ``geneset\_dir", ``colncrna\_expsets", ``morten\_geneset\_dir", \newline``morten\_expsets" and ``prepared\_expsets".

The ``geneset\_cutoff" describes the max amount of genes to be analyzed per geneset, if it is set to 0, all genes from a geneset will be included. When this cutoff is limited, the top $n$ amount of genes will be used (either the top $n$ entries or $n$ entries based on expression value variance).
``lncrna2goa\_significance" describes the FDR significance to be used for the TopoICSim predictions and output annotation files, however, the annotation files will still have their full non-cutoff counterparts available as well.

The ``geneset\_dir" represents the directory containing genesets from the HALLMARK project and ``morten\_geneset\_dir" genesets without any headers in raw text format.
 ``colncrna\_expsets", ``morten\_expsets" and ``prepared\_expsets" contain expression sets in various formats, whereof ``prepared\_expsets" can be directly loaded by the pipeline.

\section{Quality measure classifier for GO term predictions}

To test for biases and prediction quality based on selected TopoICSim values, a classifier is used.
Random forest is used as the classifier for quality measure prediction for its comprehensibility and relatively good speed. The ZeroR algorithm will also be tested as a control for the random forest algorithm. ZeroR takes the most common result and applies it to all situations regardless of any of the classifiers.\supercite{zeror}

The expression data used is the GSE63733 dataset \supercite{GSE63733} and the FANTOM5 dataset\supercite{f5}. Both these datasets contain multiple tissues and should give good coverage for predictions.
The genesets used for the analysis include four HALLMARK genesets (DNA\_REPAIR, G2M\_checkpoint, GLYCOLYSIS, and IL6\_JAK\_STAT3\_SIGNALING) and 3 custom genesets.\supercite{hall_gensets}
The 3 custom genesets are generated based on differing information contents of genes to test robustness, as they cover more of the GO DAG structure.
They are generated with the ``go\_geneset.R" R script with the following command: ``Rscript go\_geneset.R \texttt{"}/\texttt{\~{}}\texttt{"}" and consequently generates 3 genesets each consisting of 150 genes: ``custom\_geneset\_low.txt" (first 150 genes with lowest IC), ``custom\_geneset\_avg.txt" 150 genes with lowest median and ``custom\_geneset\_high.txt" last 150 genes with highest IC.

For gathering classification data for the random forest algorithm, the GO term prediction pipeline is used (Figure \ref{fig:snekpipe}), to calculate re-annotation scores in a scalable way.
To ensure annotation-bias\supercite{topoicsim} does not occur, any parameters that might increase/decrease the amount of annotation terms are left alone.
The final annotations include three outputs for differing enrichment cutoffs, 50, 250 and 450.
This is to capture differing enriched GO terms and make the annotations more diverse.
All annotation outputs are generated with the pipeline which can be run with e.g. the following command: ``snakemake --snakefile start.snakefile --cores 4".
For all benchmark calculations, only the top 15 most variant genes for each geneset are annotated, netting, theoretically, 105 ($7\times15$) annotations per ontology per expression set (630 total per enrichment cutoff). This is also done 3 times for each enrichment cutoff, netting in total, 1890 annotation outputs.

The random forest classification algorithm was used with Weka version 3.8.3\supercite{weka} on a table with general statistics of the outputted predictions.
These statistics per gene re-annotation include the minima, maxima, median, mean, variance, standard deviation of all p-values, FDR-values, and TopoICSim intraset similarity (of the re-annotation).
The mean information content (IC) per re-annotation is also used, this is based on the predicted annotation (for every transcript/gene).

The annotation outputs are aggregated with the ``pipeline\_output\_aggregation.R" script: ``\textit{Rscript pipeline\_output\_aggregation.R \texttt{"}runs\_finalbench/outraw/\texttt{"} \texttt{"}~/outcsv.csv\texttt{"} \newline\texttt{"}~/outcsv\_weka.csv\texttt{"}}"
With this the output of all analyses are aggregated into a data frame with stats per gene on each row to the ``outcsv.csv" and ``outcsv\_weka.csv" files. These files include the previously mentioned statistics for classification. These output files can be found in the attachments under: \newline``ATTACHMENTS/Data/prediction\_benchmarks/outputs/pipeline\_aggregation/".

The main reason for these statistics specifically, is that some of these directly describe meaningful relationships between GO terms, such as the IC and intraset similarity statistics.
The statistics on the p/FDR-values possibly show similar trends with certain GO term compositions.
All statistics including anything about annotation size (the amount of annotation GO terms) is excluded, as well as the data being generated with a corrected P-value no higher than 0.05 to exclude annotation length bias.\supercite{topoicsim}
With all this information on the outputted prediction, there should be a meaningful prediction accuracy of the quality measure.

\newpage
For determining how ``good" a classification algorithm is, the ``mean absolute error" is used.
Mean absolute error shows the average amount of error between predicted quality versus real quality, thus quantifying this difference.\supercite{meanabso}

\section{lncRNA and gene comparison in both prostate and colorectal cancer}

Not only is directly scoring quality of annotation important, but also the actual comparison of already known cancer-related functionalities with annotation predicted lncRNAs.
This is a hard problem to tackle however, since generally the amount of annotated GO terms seems to be varying between ~0-200 GO terms.
To accommodate the plurality of these GO terms, they need to be aggregated somehow into more general groups and relationships.

By using TopoICSim similarities within a certain geneset(s) and doing PCoA, one can determine an arbitrary N-dimensional Euclidean space which can be used for further distance analyses.
One of such analyses would include K-means clustering, which can be used to determine an N amount of clusters within TopoICSim's PCoA derived euclidean coordinates.

\subsection{K-means/auto K-means}

Since K-means needs user input on how many $K$ clusters to generate, it cannot be used directly for automatically determining $K$.
However, this can be done by determining the percentage of variance the $K$ clusters catch out of the original dataset.
This variance percentage can be achieved by calculating the sum of squares for the original data and the centroid data the K-means output gives:

\begin{equation} \label{eq:6}
S = ( \,\frac{\sum_{i=1}^{n}( \, x_i - \bar{x}) \,^2}{\sum_{j=1}^{n}( \, y_j - \bar{y}) \,^2}) \,\times 100
\end{equation}

In \cref{eq:6} where dataset $X$ represents the original data and $Y$ the centroid data, score $S$ is calculated, which acts as a threshold value for our $K$ clusters.
This whole process is called ``auto K-means".

To describe the different K-means clusters, the top 5 most frequent GO terms of their underlying members are used, this way, the GO term data can be summarized.

\subsection{lncRNA-KEGG geneset  analysis}

For this analysis on both prostate and colorectal cancer, two main expression datasets are used: TCGA and Fantom5.
The TCGA data is specific for both cancers and are separated into the prostate group (TCGA-PRAD) and colorectal cancer group (TCGA-COAD).
Fantom5 is used as a control group for both analyses and provides expression data in ~1800 tissue samples.\supercite{f5,TCGA} All expression values are FPKM corrected.

The lncRNA's and genesets that are used are also separate for both cancers.
For prostate cancer, the ``KEGG\_PROSTATE\_CANCER" geneset from the HALLMARK database is used, with the lncRNA's from ``Yuichi Mitobe et al."\supercite{mitobe_prostate}.
For colorectal cancer, the \\``KEGG\_COLORECTAL\_CANCER" (also from HALLMARK) is used together with the lncRNA's from ``Haas GP et al."\supercite{haas_prostate}.

Like in the benchmark analysis, the pipeline in Figure \ref{fig:snekpipe} is used to determine annotation predictions. The full pipeline was run, even though only the annotation predictions are relevant for further analysis.
Because this pipeline is used, SYMBOL IDs are corrected here as well. 

The K-means clustering analysis is done with the auto-kmeans method, automatically determining the $K$ to be used. The percentage variance threshold is at 0.45 (45\%), a little under half the variance, this is arbitrarily chosen. Then, the top 5 most frequent GO terms of each K group is used for determining TopoICSim similarities between the clusters.

To group together lncRNAs across both expressionsets, hierarchical clustering is done on top of all Kmeans groups. This is done on the TopoICSim similarities.
The hierarchical clustering is cut off with an $N$ of 5 (so max, 5 groups), so that all different groups can be clustered together to a reasonable amount, this is also arbitrarily chosen.

\subsection{Ontological relationships}

With K-means clusters summarized, ontological relationships may appear in the various clusters among both the Fantom5 data set and the TCGA counterpart.
This could be represented by K-means clusters grouping together genes and lncRNAs with similar functions and properties.
Hierarchical clustering can add onto this and perhaps highlight slight differences between K-groups across both expression data sets. This might uncover differences between ``normal" and cancer tissue, as well as give a more globalized view of the groups.

\subsection{Method composition}

To test differences between lncRNA annotation predictions and those of coding genes, percentile composition of each correlation method from the lncRNA2GOA algorithm is also calculated. The lncRNA and KEGG transcripts/genes will function as separate groups.

\section{Development and deployment environment}

The benchmarking/annotation pipeline was built with Snakemake 5.4.4 (with Python 3.6.8) and R 3.5.1 (with Rstudio 1.2.1335)\supercite{snakemake,python,rproj,rstud}. These used a multitude of libraries and packages. The Python libraries used are listed in Table \ref{tab:pypack} as well as in the attachment file ``ATTACHMENTS/havpryd\_snakemake\_info.txt". The R packages and session info is too large to be included here, but is available in the attachment file ``ATTACHMENTS/havpryd\_sessioninfo.txt". As stated earlier, the random forest algorithm used Weka version 3.8.3\supercite{weka}.

All analyses done were run under the ``Ubuntu 12.04.5 LTS" Operating system with Linux kernel ``GNU/Linux 3.13.0-117-generic x86\_64". Further reading about exact commands and configurations used can be found in the repository Readme for this project: ``\url{https://bitbucket.org/Berghopper/lncrna_thesis_prj/src/master/}".
The attachment of this project can also be downloaded from this repository.
It contains all input, output, and intermediaries done in all analyses (as well as some project files).

\begin{table}[h!]
	\small
	\begin{center}
		\caption{\textit{Table describing all python libraries used with the snakemake annotation prediction benchmarking pipeline.}}
		\label{tab:pypack}
		\rowcolors{1}{white}{gray!25}
		\begin{tabular}{|l|l|} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
			\textbf{Python library} & \textbf{Version}\\
			\hline
			appdirs & 1.4.3\\
			attrs & 19.1.0\\
			certifi & 2019.3.9\\
			chardet & 3.0.4\\
			ConfigArgParse & 0.14.0\\
			datrie & 0.7.1\\
			docutils & 0.14\\
			gitdb2 & 2.0.5\\
			GitPython & 2.1.11\\
			idna & 2.8\\
			jsonschema & 3.0.1\\
			numpy & 1.16.2\\
			pandas & 0.24.2\\
			pyrsistent & 0.14.11\\
			python-dateutil & 2.8.0\\
			pytz & 2018.9\\
			PyYAML & 5.1\\
			ratelimiter & 1.2.0.post0\\
			requests & 2.21.0\\
			six & 1.12.0\\
			smmap2 & 2.0.5\\
			snakemake & 5.4.4\\
			urllib3 & 1.24.1\\
			wrapt & 1.11.1\\
			yappi & 1.0
		\end{tabular}
	\end{center}
\end{table}

