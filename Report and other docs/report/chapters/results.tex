\section{Classification of TopoICSim Intraset similarity as a quality measure}

\subsection{Data coverage after analysis}

Out of the total 1890 theoretical predictions, 1770 were recovered (93.65\%) to have annotations and TopoICSim scores available.
Due to several reasons, some of the annotations could be empty, most likely due to not having expression or GO term data available.

\subsection{Random Forest classification performance}

\begin{figure}[h!]
	\centering
	\includegraphics[height=330px]{images/zeror.png}
	\caption{\textit{Screenshot of Weka 3.8.3 showing the ZeroR results for the classification benchmark test. The mean absolute error is 0.1846 with 0 correlation (as is inherent of the ZeroR method).}}
	\label{fig:zeror_result}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[height=330px]{images/trees_rdfor.png}
	\caption{\textit{Screenshot of Weka 3.8.3 showing the results for the random forest algorithm on the benchmark data. The mean absolute error is 0.1312 with a correlation of 0.6365.}}
	\label{fig:tree_result}
\end{figure}

\newpage
Figure \ref{fig:zeror_result} shows the ZeroR as the baseline classifier, with the mean absolute error at 0.1846. In Figure \ref{fig:tree_result}, the random forest algorithm is shown with a mean absolute error of 0.1312 and a correlation coefficient of 0.6365.

\section{lncRNA analysis}

\subsection{Coverage and usable lncRNAs}

Because of the conversion into update SYMBOL IDs, some lncRNAs might no be present in this analysis as an output.
The amount of lncRNAs that are usable in the analysis versus the original amount (coverage) is described here.
Not only is the coverage lowered by symbol conversion, but it is also affected by expression and GO term data. If a prediction is not found to be significant, it is scrapped as well.
Coverages are different for both prostate and colorectal cancer, expressionset and ontology. The coverage percentages are described in Table \ref{tab:cov}.
Since coverage percentages are very low in the 0.05 significance cutoff, from here on out, mostly 0.10 cutoff percentages will be discussed, as the results would otherwise not contain enough lncRNAs to discuss. Prostate cancer seems to have a higher coverage of lncRNAs than colorectal cancer as well.

\newpage
\begin{table}[h!]
	\small
	\begin{center}
		\rowcolors{1}{white}{gray!25}
		\caption{\textit{Table describing coverage percentage per cancer group. Each cancer group has been ran with all 3 GO ontologies with 0.10 and 0.05 significance intervals. The coverage describe the percentage of original lncRNAs present in the final clustering analyses results.}}
		\label{tab:cov}
		\begin{tabular}{|l|l|l|l|} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
			\textbf{Cancer group} & \textbf{Expressionset} & \textbf{Ontology} & \textbf{(Coverage \%)}\\
			\hline
			Colon 0.05 & FANTOM 5 & MF & 38.89\\
			Colon 0.05 & FANTOM 5 & BP & 38.89\\
			Colon 0.05 & FANTOM 5 & CC & 38.89\\
			Colon 0.05 & TCGA-COAD & MF & 6.94\\
			Colon 0.05 & TCGA-COAD & BP & 16.67\\
			Colon 0.05 & TCGA-COAD & CC & 5.56\\
			Colon 0.10 & FANTOM 5 & MF & 38.89\\
			Colon 0.10 & FANTOM 5 & BP & 38.89\\
			Colon 0.10 & FANTOM 5 & CC & 38.89\\
			Colon 0.10 & TCGA-COAD & MF & 30.56\\
			Colon 0.10 & TCGA-COAD & BP & 30.56\\
			Colon 0.10 & TCGA-COAD & CC & 19.44\\
			Prostate 0.05 & FANTOM 5 & MF & 52.94\\
			Prostate 0.05 & FANTOM 5 & BP & 52.94\\
			Prostate 0.05 & FANTOM 5 & CC & 52.94\\
			Prostate 0.05 & TCGA-PRAD & MF & 2.94\\
			Prostate 0.05 & TCGA-PRAD & BP & 20.59\\
			Prostate 0.05 & TCGA-PRAD & CC & 5.88\\
			Prostate 0.10 & FANTOM 5 & MF & 52.94\\
			Prostate 0.10 & FANTOM 5 & BP & 52.94\\
			Prostate 0.10 & FANTOM 5 & CC & 52.94\\
			Prostate 0.10 & TCGA-PRAD & MF & 35.29\\
			Prostate 0.10 & TCGA-PRAD & BP & 44.12\\
			Prostate 0.10 & TCGA-PRAD & CC & 23.53 
		\end{tabular}
	\end{center}
\end{table} 

\subsection{K-means clustering results}

For the K-means clustering analysis, plots are created for every cancer group $\times$ expression set $\times$ ontology, thus not all plots will be included in the report directly.
The visualization is just that - a visualization, not a conclusion of said K-means clustering.
To give an example of one such visualization, the Prostate 0.10 group with the TCGA-PRAD dataset on the MF ontology is plotted in Figure \ref{fig:kex}.

A summary for MF ontology based clustering on each cluster can be found in Figure \ref{tab:kgopros} for prostate cancer and Figure \ref{tab:kgocol} for colorectal cancer.

\newpage
\begin{figure}[h!]
	\centering
	\includegraphics[height=240px]{images/01.png}
	\caption{\textit{Example plot of a K-means clustering visualization on the Prostate 0.10 group with the TCGA-PRAD dataset on the MF ontology. The plot plots the first two principle components of the PCoA analysis, showing only the distances on these two dimensions. Here it is clearly shown that lncRNAs are forming and merging with other groups, the lncRNAs are highlighted with black text.}}
	\label{fig:kex}
\end{figure}

A multitude of tables is available describing these clusters in the attachment: ``ATTACHMENTS/Data/lncRNA\_annotation/outputs/topoicsim\_intraset\_analysis/clustering\_output- \parskip = 0.3cm /*/summary\_tables/". For the plot above, this would be: \newline
``ATTACHMENTS/Data/lncRNA\_annotation/outputs/topoicsim\_intraset\_analysis/cluste- \\ ring\_output/lnc\_pros0.10\_out/summary\_tables/kmeans\_desc\_lnc\_analysis-TCGA\_PRAD\_all-MF.rda-lnc\_pros0.10.csv".

\subsection{Hierarchical clustering}

The hierarchical clustering should enhance the K-means clustering by combining clusters between expressionsets as well as giving a more global overview of the several functional groups in the genesets. Here, only the groups with significance 0.10 of heatmaps will be shown, these reside in Appendix 2 (Chapter \ref{chap:app2}).

In Figure \ref{fig:heatpr0.1MF}, the clustering clearly shows multiple groups of relatedness, which are also separated by each other. However, only a part remains usable, as some of the hierarchical clusters contain only very non-related K-means groups. This generally tends to be the case for all heatmap Figures (Figures \ref{fig:heatpr0.1MF}-\ref{fig:heatcl0.1CC}), but not all.

For instance, Figure \ref{fig:heatcl0.1CC} (Colorectal CC) has only 1 cluster which contains similarities higher than ~0.7 which also consist of more than 1 K-means group. Moreover, Figure (Prostate BP) does have 2 clusters, but cluster 2 remains small and can mostly thank its high similarity to the TCGA-8 and ft5-4 cluster. It also contains so many K-means clusters is becomes hard to read. Both these problems occur because of either lack of precision in the GO terms (CC ontology) or too high specificity in them (BP ontology). This forces hierarchical clustering to pick the largest similar group or the groups becoming too large for manual analysis. The MF ontology in both Figure \ref{fig:heatpr0.1MF} and \ref{fig:heatcl0.1MF} seem to meet in between with the colon clustering having 2 hierarchical clusters above 0.7 and prostate having 3 clusters above 0.7 while not containing too many clusters. 

\subsubsection*{Hierarchical cluster properties for MF ontology}

In Figure \ref{fig:heatpr0.1MF}, 3 clusters with a similarity above 0.7 are shown. These are described below on which lncRNAs are present in which groups and what the top 3 GO terms are for that specific hierarchical cluster.
Group 1 contains the lncRNAs: CDKN2B-AS1, SUZ12P1, PVT1 and is described by GO terms: ``DNA-binding transcription factor activity, RNA polymerase II-specific", ``DNA-binding transcription activator activity, RNA polymerase II-specific " and ``protein binding".
Group 2 contains:  PVT1, SNHG1, HOTTIP, APBB3 and is described by GO terms: ``protein binding", ``protein serine/threonine kinase activity" and ``ATP binding".
Group 4 contains: PCA3, H19, CTBP1-AS, PCAT1, GAS5, TRPM2-AS, MALAT1, NEAT1, DANCR, SRA1, CDKN2B-AS1, UCA1, KRASP1 and is described by GO terms: ``protein binding", ``protein heterodimerization activity" and ``identical protein binding".

The GO descriptions are derived from Table \ref{tab:hiergopros}, while the lncRNAs can be viewed more in detail in Table \ref{tab:kgopros} in which K-means group they occur.
What is apparent is that both group 2 and 4 have protein binding specific features, although they do differ from each other slightly.
Group 1 mostly shows transcription factor related activity.
Both lncRNAs PVT1 and CDKN2B-AS1 occur multiple times in both group 1 and 2, possibly showing differences between FANTOM 5 and TCGA expression sets.

\begin{table}[h!]
	\small
	\begin{center}
		\rowcolors{1}{white}{gray!25}
		\caption{\textit{Table containing descriptions of the hierarchical clustering of the prostate cancer for FDR cutoff 0.10 on the MF ontology. Here the clusters are listed together with their subsequent top 5 GO terms (descriptions not included).}}
		\label{tab:hiergopros}
		\begin{tabular}{|l|l|} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
			\textbf{Hierarchical cluster} & \textbf{GO terms}\\
			\hline
			1 & GO:0000981;GO:0001228;GO:0005515;GO:0003700;GO:0003677\\
			2 & GO:0005515;GO:0004674;GO:0005524;GO:0004672;GO:0042802\\
			3 & GO:0005201;GO:0005509;GO:0030020;GO:0031013;GO:0000981\\
			4 & GO:0005515;GO:0046982;GO:0042802;GO:0005524;GO:0005161\\
			5 & GO:0003865;GO:0004089;GO:0004467;GO:0005230;GO:0005324
		\end{tabular}
	\end{center}
\end{table} 

In Figure \ref{fig:heatcl0.1MF} 2 clusters with a similarity above 0.7 are shown. These are described below on which lncRNAs are present in which groups and what the top 3 GO terms are for that specific hierarchical cluster.
Group 1 contains the lncRNAs: MALAT1, NEAT1, ZFAS1, PVT1, DANCR, CYTOR, GAS5, CRNDE, TUG1, CDKN2B-AS1, TINCR, FTX, LINC01527, UCA1 and is described by GO terms: ``protein binding", ``protein serine/threonine kinase activity" and ``ATP binding".
Group 2 contains: HOTTIP, ZFAS1, PVT1, HULC, AOC4P, H19, MEG3, HOTAIR, TUG1, NEAT1, SNHG20, FTX, CASC2 and is described by GO terms: ``extracellular matrix structural constituent", ``calcium ion binding" and ``extracellular matrix structural constituent conferring tensile strength".

The GO descriptions are derived from Table \ref{tab:hiergocol}, while the lncRNAs can be viewed more in detail in Table \ref{tab:kgocol} in which K-means group they occur.
Group 1 shows protein binding properties while group 2  shows properties towards tensile strength of the extracellular matrix.
lncRNAs NEAT1, ZFAS1, PVT1, TUG1, FTX occur multiple times in both group 1 and 2, possibly showing differences between FANTOM 5 and TCGA expression sets.

\begin{table}[h!]
	\small
	\begin{center}
		\rowcolors{1}{white}{gray!25}
		\caption{\textit{Table containing descriptions of the hierarchical clustering of the colorectal cancer for FDR cutoff 0.10 on the MF ontology. Here the clusters are listed together with their subsequent top 5 GO terms (descriptions not included).}}
		\label{tab:hiergocol}
		\begin{tabular}{|l|l|} % <-- Alignments: 1st column left, 2nd middle and 3rd right, with vertical lines in between
			\textbf{Hierarchical cluster} & \textbf{GO terms}\\
			\hline
			1 & GO:0005515;GO:0004674;GO:0005524;GO:0004672;GO:0016301\\
			2 & GO:0005201;GO:0005509;GO:0030020;GO:0031013;GO:0000981\\
			3 & GO:0000981;GO:0005515;GO:0042802;GO:0003677;GO:0046982\\
			4 & GO:0015333;GO:0035673;GO:1904315;GO:0004970;GO:0005509\\
			5 & GO:0008392;GO:0055102;GO:0060228;GO:0004497;GO:0004867
		\end{tabular}
	\end{center}
\end{table}

\newpage
\subsection{Correlation method composition}

In Figure \ref{fig:cumu10} cumulative percentages are shown between correlation methods, for both the lncRNA and protein-coding (``other transcripts") KEGG genes. ``Fisher", ``Pearson" and ``Sobolev" all occur less in the lncRNA annotation predictions, while ``Kendall" and ``Spearman" are more frequent. These cumulative percentages are based on the 0.10 FDR cutoff, meaning only annotation predictions with this significance are present in this frequency analysis.

\begin{figure}[h!]
	\centering
	\includegraphics[height=400px]{images/method_composition_0_10.png}
	\caption{\textit{Plot of cumulative percentages on method composition used in the correlation lncRNA cancer analysis for the 0.10 FDR cutoff. The cumulative percentages are shown separately for lncRNAs and other transcripts, the other transcripts include only protein coding genes from the KEGG genesets. On the right, a legend is shown for the used correlation method labeled as ``used\_method".}}
	\label{fig:cumu10}
\end{figure}
