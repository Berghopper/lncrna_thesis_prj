# README FOR "Analysis of non-coding transcripts related to prostate and colorectal cancer"

# Author

* Casper van Mourik @Berghopper

# Acknowledgements

The project has been supervised by Finn Drabløs and Morten Rye from the NTNU Department of Molecular science, Bioinformatics and gene regulations group.

Supervision and evaluation was also provided by Martijn Herber and Jasper Bosman from Hanze Hogeschool Groningen, Institute of Life Science & Technology.

# LICENSE

Copyright (c) [2019-2020] [Casper van Mourik] under the MIT license.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## **Project report and attachment**

The thesis report can be found on the [downloads page](https://bitbucket.org/Berghopper/lncrna_thesis_prj/downloads/) of the repository.
Since the attachment is too large for bitbucket, it instead resides on [google drive](https://drive.google.com/open?id=1Ql8SxbVpQe9YEyeOuR5xCx-0pd4dY9sf).

## 1. About

This project aims to better benchmark newly discovered correlation metrics for predicting annotations on lncRNA (with the R package GAPGOM). These correlation methods work specifically for transcript expression data. With this, the project also tries to uncover any biases in the lncRNAs annotation predictions, while also analysing homology and gene relatedness.

The `src` folder contains all code and scripts, it has its own [Readme](src/Readme.md) explaining all the different subdirectories/code/codefiles.

Each code folder should have it's own readme, describing the individual files.
Some code is ran by a snakemake pipeline, so that it may be sped up with multi-core performance.

In terms of workflow, the [Report](Report and other docs/report/lncrna_report.pdf) details more on this and similar structure will be kept troughout this readme for describing ran analyses.

The [Report and other docs](Report and other docs/) folder contains the following:

- `report` contains report tex files.
- `misc` Miscellaneous files, presentation etc.

## 2. Generated data

The data that has been generated together with a copy of this repository, is present in the `ATTACHEMENTS.zip` file in the downloads of this repository. This will be documented further in this clause, using the `ATTACHEMENTS` directory as a root directory for most following commands

### 2.1 General data preparation (`src/data_n_prep`)

This subheading will contain information about generated expressionsets and listed softwares used.
Since the expressionsets are used in most analyses, they are listed here.

For translation between ENSEMBL and Gene Name (SYMBOL) ids, Biomart on the ensembl website was used.
The file `ATTACHMENTS/Data/mart_export.txt` is used in the following commands.

#### 2.1.1 Fantom 5 expression data

The Fantom 5 data is an expression data consortium containg ~1600 samples of different tissues in both the mouse (mm9) and human (hg19) species.
For this project, the [symbol based data-file](http://fantom.gsc.riken.jp/5/datafiles/latest/extra/gene_level_expression/hg19.gene_phase1and2combined_tpm.osc.txt.gz) is used.

Fantom 5 is downloaded via the following command:

`wget http://fantom.gsc.riken.jp/5/datafiles/latest/extra/gene_level_expression/hg19.gene_phase1and2combined_tpm.osc.txt.gz`

and unzipped with:

`gunzip hg19.gene_phase1and2combined_tpm.osc.txt.gz`

This txt file contains a tabular like format, with a special header describing column names.
The GAPGOM package is used for parsing this data with the `fantom_load_raw()` command.

Afterwards, the dataframe is parsed such that SYMBOL ids are updated with the `limma` package's `alias2SymbolTable` function, these convert old SYMBOL synonyms to their modern counterpart.

Any SYMBOL IDs that occur twice, are parsed such that only the most variant row of expression values is kept.
All rows without correct/current SYMBOL definitions do not change their ID, but are kept (unless they have duplicated like stated previously).
However, in the benchmarking analysis an older parsed version of this dataset was used which did not keep such rows at all, instead omitting them.

The resulting file(s) are generated using the following commands:

- Original expset: `Rscript fantom5_genes.R "~/Downloads/hg19.gene_phase1and2combined_tpm.osc.txt" "~/ft5_expset.rda" "TRUE"`
- expset with all transcripts `Rscript fantom5_genes.R "~/Downloads/hg19.gene_phase1and2combined_tpm.osc.txt" "~/ft5_expset_all.rda" "FALSE"`

#### 2.1.2 TCGA

TCGA (The Cancer Genome Atlas) is a database containing 33 cancer types with various kinds of data.
For this particular project, the following filters where applied on the [repository](https://portal.gdc.cancer.gov/repository):

Transcriptome Profiling, Gene Expression Quantification, RNA-Seq, HTSeq - FPKM for both the TCGA-COAD (colorectal cancer) and TCGA-PRAD (prostate cancer) datasets.

After downloading all these files into a `.tar.gz` archive, the files can be unpacked with the following commands to all occur in the same directory `all_exprs`:

`mkdir all_exprs && mkdir all_exprs/unpack && cd all_exprs/unpack && mv ../../gdc_download*.tar.gz ./ && tar -zxvf ./gdc_download*.tar.gz && mv MANIFEST.txt ../../ && mv ./gdc_download*.tar.gz ../../ && cd .. && mv unpack/*/*.gz ./ && rm -rf ./unpack && gunzip ./* && cd ..`

`all_exprs` folder is used by the `src/data_n_prep/expsets/TCGA_sets.R` command to generate the expressionsets.

two seperate directories contain the `all_exprs` folder: the `prostate_TCGA-PRAD` folder (prostate cancer) and the `colon_TCGA-COAD` folder (colorectal cancer).

Similar to the FANTOM 5 data, seperate datasets were generated.
One for the benchmarking analysis (containing only transcripts which have a corresponding SYMBOL ID) and one for the rest of the analyses.
The one used for the lnc analyses have transcripts without corresponding SYMBOL's use their old versioned ENSEMBL ID (the one in the original dataset).
The generating uses [BioMart](https://www.ensembl.org/biomart/martview/) for converting between ENSEMBL-SYMBOL (Ensembl Genes 96, Human genes GRCh38.p12, and Gene stabl ID (ENSEMBL) + Gene name (SYMBOL)).
For instances with duplicate SYMBOL IDs the most variant row of expression values was allowed to keep its SYMBOL, all others are reverted to their old ENSEMBL ID (or omitted with the original).
The versioned ENSEMBL IDs are generated by cutting of their versioned number, this allowed for more data to be used.

The resulting file(s) are generated using the following commands:

##### Prostate:

- Original expset: `Rscript TCGA_sets.R "~/Downloads/prostate_TCGA-PRAD/all_exprs/*" "~/Downloads/mart_export.txt" "~/TCGA_PRAD_12_04_2019.rda" "TRUE"`
- expset with all transcripts `Rscript TCGA_sets.R "~/Downloads/prostate_TCGA-PRAD/all_exprs/*" "~/Downloads/mart_export.txt" "~/TCGA_PRAD_all.rda" "FALSE"`

##### Colorectal: 

- Original expset: `Rscript TCGA_sets.R "~/Downloads/colon_TCGA-COAD/all_exprs/*" "~/Downloads/mart_export.txt" "~/TCGA_COAD_12_04_2019.rda" "TRUE"`
- expset with all transcripts `Rscript TCGA_sets.R "~/Downloads/colon_TCGA-COAD/all_exprs/*" "~/Downloads/mart_export.txt" "~/TCGA_COAD_all.rda" "FALSE"`

The original expsets, are only used in the performance benchmarks (2.2), the new ones are used in the lncRNA analysis (2.3).

### 2.2 Performance benchmarks on new correlation metrics (`reliability_measure`)

#### 2.2.1 Gene/transcript list generation

To have a good coverage on different part of the GO annotion structure, multiple genesets are generated based on their information content (this is done by `go_geneset.R`). Also multiple HALLMARK genesets have been used in the analysis:

- DNA_REPAIR
- G2M_checkpoint
- GLYCOLYSIS
- IL6_JAK_STAT3_SIGNALING

The hallmark genesets have been used because they have probably good annotations and are well known.

Three artificial genesets have been generated, all with 150 genes in them, based on their IC value (lowest, highest and mean).

These are to be generated with the `go_geneset.R` script and can be reproduced with the following command:

`Rscript go_geneset.R "~/"`

(This will create the genesets in the home directory.)

There's no input required besides output directory, where three files will be written with the corresponding genesets:

- `custom_geneset_low.txt`
- `custom_geneset_avg.txt`
- `custom_geneset_high.txt`

It also bases the geneset of 1 other factor: the amount of GO terms a gene consists of.
This gets filtered out such that a gene has to have anywhere between 2-10 GO terms.
This has been selected to filter out under and overannotated genes.

All calculations are based on the MF ontology because this is the most "precise" ontology.

#### 2.2.2 Pipeline runs

For running the pipeline, the following command was used in the pipeline's directory:

```
echo $(date) && snakemake --snakefile start.snakefile --cores 32 -pr --runtime-profile ~/timings.txt || true && echo $(date)
```

Three different settings were used in the configuration file; The variable `lncrna2goa_enrichment_cutoff` was defined with "50", "250" and "450".

The following other variables differ in the benchmark done:

- `geneset_cutoff` - limited to the top 15 genes of each geneset.
- `include_anno_only` - was set to "F" (false), all genes were included in the analysis.

all 5 input directories were also altered, these were directed to the 5 directories with the same names in `ATTACHMENTS/Data/prediction_benchmarks/inputs/final_bench/`.

#### 2.2.3 Pipeline output aggregation

To try and make a quality measure, we will use topoicsim similarities as a quality measure to be classified.
For this we need to generate a test set where we can run a classifier on.
This test set will contain information on the annotation prediction and the topoicsim result (as the value to be classified).

`Rscript pipeline_output_aggregation.R "ATTACHMENTS/Data/prediction_benchmarks/outputs/pipeline_outputs/" "ATTACHMENTS/Data/prediction_benchmarks/outputs/pipeline_aggregation/outcsv.csv" "ATTACHMENTS/Data/prediction_benchmarks/outputs/pipeline_aggregation/outcsv_weka.csv"`

Here we generate 2 outputs, one with more information and statistics than the other:

- `outcsv.csv` - csv with all statistics on annotation prediction and results
- `outcsv_weka.csv` - csv with reduced statistics (only annotation prediction and topoicsim result), statistics on the result have been left out (as these are not available with a case whereof the annotation isn't known yet) as well as information regarding the length of the annotation prediction. The length or amount of predicted GO terms can cause a prediction to be biased because of annotation length bias.
This bias occurs when a large amount of GO terms are used for similarity calculations because they tend to overlap more space in the GO tree.

Weka is used with the random forest algorithm to generate a prediction model.

### 2.3 lncRNA analysis (`src/lnc_analysis`)

#### 2.3.1 Pipeline runs

Similar to the benchmark, the same config and commands were used:

```
echo $(date) && snakemake --snakefile start.snakefile --cores 32 -pr --runtime-profile ~/timings.txt || true && echo $(date)
```

None of the major configuration settings were altered. All annotation results for all significances are kept in the `custom_anno/*.RAW` files, which can be cut off later.

For the 5 input directories, `ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/` and `ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/` were used with the same folder names.

All these pipeline outputs are contained in `ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/` for both cancer types.

#### 2.3.2 Intraset similarity

The TopoICSim intraset similarities are calculated given the input annotation (`custom_anno/*.txt.RAW`) files.
The intraset similarity matrices are calculated together with some small extra statistics and objects:

- `result` - TopoICSim result
- `all_genes` - all genes used in analysis
- `all_custom_genes` - all lncRNA labels
- `lnc_translation_table` - translation table between original input geneset and converted symbols (updated standard with `limma` package)
- `lnc_coverage` - the coverage of original lncRNA's that get annotated
- `lnc_symbol_coverage` - the coverage after the initial SYMBOL conversion

The results are calculated with the following commands, for a significance precision of 0.05 and 0.10 (FDR cutoff for annotations), all ontologies, on both prostate and colorectal cancer:

```
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lcn_pros/" "0.05" "5" "MF"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lcn_pros/" "0.05" "5" "BP"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lcn_pros/" "0.05" "5" "CC"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lnc_colon/" "0.05" "5" "MF"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lnc_colon/" "0.05" "5" "BP"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lnc_colon/" "0.05" "5" "CC"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lcn_pros0.10/" "0.10" "5" "MF"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lcn_pros0.10/" "0.10" "5" "BP"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_prostate/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lcn_pros0.10/" "0.10" "5" "CC"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lnc_colon0.10/" "0.10" "5" "MF"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lnc_colon0.10/" "0.10" "5" "BP"
Rscript ./topo_lnc.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/lnc_colon/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/intrasetsim/lnc_colon0.10/" "0.10" "5" "CC"
```

These commands output 12 files (per cancer), 6 for each ontology and expression set. These outputs are contained in 4 output directories, 2 per cancer for each FDR cutoff.

#### 2.3.3 Clustering and plotting

Clustering + plotting is done by `clustering.R` with the following 4 commands (based on the topoicsim output directories):

```
Rscript clustering.R "lnc_pros0.10" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/clustering_output/lnc_pros0.10_out" "lnc_pros0.10" 
Rscript clustering.R "lnc_colon0.10" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/clustering_output/lnc_colon0.10_out" "lnc_colon0.10" 
Rscript clustering.R "lnc_pros" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/clustering_output/lnc_pros_out" "lnc_pros"
Rscript clustering.R "lnc_colon" "ATTACHMENTS/Data/lncRNA_annotation/outputs/topoicsim_intraset_analysis/clustering_output/lnc_colon_out" "lnc_colon" 
```

#### 2.3.4 Method coverage

For plotting the method coverage plots, the `method_coverage.R` script is used for both FDR cutoffs.
`ATTACHMENTS/Data/lncRNA_annotation/inputs/all_lncs.csv` is used for providing information on which exact SYMBOL IDS were used for the lncRNAs in both cancer pipelines.

```
Rscript ./method_coverage.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/all_lncs.csv" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/method_composition_0.05.png" "0.05"
Rscript ./method_coverage.R "ATTACHMENTS/Data/lncRNA_annotation/inputs/all_lncs.csv" "ATTACHMENTS/Data/lncRNA_annotation/outputs/pipeline_outputs/" "ATTACHMENTS/Data/lncRNA_annotation/outputs/method_composition_0.10.png" "0.10"
```

