# Script to load all used libs in the whole project, and print out the session
# info.

library(GAPGOM)
library(tools)
library(ggplot2)
library(Cairo)
library(Biobase)
library(limma)
library(data.table)
library(BiocGenerics)
library(ggrepel)
library(ape)
library(plotly)
library(RColorBrewer)
library(AnnotationDbi)
library(Matrix)
library(gplots)

sessionInfo()