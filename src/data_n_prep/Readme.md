# Readme for miscellaneous data generation (expression sets, libraries used)

## Expressions sets (`expsets` folder)

For both Fantom 5 and TCGA data, expressionsets are prepared seperately based on SYMBOL ids for the benchmarking/annotation pipeline.
Full commands used can be found in the main readme and the report.

- `fantom5_genes.R` - generates expressionset R object for FANTOM 5 data
- `TCGA_sets.R` - generates expressionset R object for TCGA datasets

## Misc scripts
- `ALLLIBS.R` - generates `sessioninfo()` output