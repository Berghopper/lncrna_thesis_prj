library(tools)
library(GAPGOM)

rm(list=ls())

# BASAL INPUTS (args)
# 1 - base_input_dir (pipeline input folder)
# 2 - base_output_dir (pipeline output folder)
# 3 - analysis_out (output directory for this analysis)
# 4 - go_min_fdr (min fdr cutoff)
# 5 - max_go (max allocated GO terms per lncRNA)
# 6 - onto (ontology)

options(stringsAsFactors = FALSE, digits = 22)
data.table::setDTthreads(1)

args <- commandArgs(trailingOnly = TRUE)

# ARGS

base_input_dir <- args[1]
base_output_dir <- args[2]
analysis_out <- args[3]
go_min_fdr <- as.numeric(args[4])
max_go <- as.numeric(args[5])
onto <- args[6]

dir.create(file.path(analysis_out), showWarnings = FALSE)

anno_out_dir <- paste0(base_output_dir, "/custom_anno/")

expsets <- Sys.glob(paste0(base_output_dir, "/prepped_datasets/expsets/*"))
expsets <- basename(file_path_sans_ext(expsets))

genesets <- Sys.glob(paste0(base_output_dir, "/prepped_datasets/genesets/*"))
genesets <- basename(file_path_sans_ext(genesets))
genesets <- unlist(strsplit(genesets, "-"), FALSE, FALSE)
genesets <- unique(genesets[seq(2,length(genesets),2)])

ontologies <- c("MF", "CC", "BP")

# for each geneset, calculate all topo sims (lnc-custom x kegg)
for (expset in expsets) {
  # only get custom gos if "lnc" is in the filename of geneset
  lnc_genesets <- genesets[pmatch("lnc", genesets)]
  other_genesets <- genesets[-pmatch("lnc", genesets)]
  
  all_genes <- c()
  
  for (other_geneset in other_genesets) {
    geneset_filename <- paste0(base_output_dir, "/prepped_datasets/genesets/", expset, "-",  other_geneset, ".txt")
    all_genes <- c(all_genes, read.table(geneset_filename)$x)
  }
  
  all_custom_genes <- list()
  lnc_genes_original <- c()
  
  for (lnc_geneset in lnc_genesets) {
    custom_genes <- list()
    
    original_geneset_filename <-  paste0(base_input_dir, "/genesets_morten/", lnc_geneset, ".txt")
    lnc_genes_original <- c(lnc_genes_original, read.table(original_geneset_filename)[[1]])
    
    geneset_filename <- paste0(base_output_dir, "/prepped_datasets/genesets/", expset, "-",  lnc_geneset, ".txt")
    lnc_genes <- read.table(geneset_filename)$x
    
    for (gene in lnc_genes) {
      go_anno <- read.table(paste0(anno_out_dir, expset, "-", lnc_geneset, "-human-", onto, "-", gene, ".txt.RAW"))
      if (!all(is.na(go_anno))) {
        if (go_anno[[1]][1] != "GOID") { # check if header is not read as value (this means an empty table)
          go_anno <- go_anno[go_anno$FDR < go_min_fdr,]
          
          if (max_go < nrow(go_anno)) {
            custom_genes[[gene]] <- go_anno[1:max_go, 1]  
          } else if (nrow(go_anno) > 1) {
            custom_genes[[gene]] <- go_anno[, 1]
          }
        }
      }
    }
    all_custom_genes <- c(all_custom_genes, custom_genes)
  }
  # topo
  print("Starting analysis:")
  print(expset)
  print(onto)
  # topo result
  result <- topo_ic_sim_genes("human", onto, all_genes, all_genes, custom_genes1 = all_custom_genes, custom_genes2 = all_custom_genes, idtype = "SYMBOL")
  # coverage of original lnc geneset(s)
  
  lnc_symbol_missing <- sum(is.na(limma::alias2SymbolTable(lnc_genes_original)))
  lnc_symbol_coverage <- ((length(lnc_genes_original)-lnc_symbol_missing)/length(lnc_genes_original))*100
  
  
  lnc_translation_table <- as.data.frame(sapply(lnc_genes_original, function(gene) {paste0(limma::alias2Symbol(gene), collapse = ", ")}))
  colnames(lnc_translation_table) <- "Converted Symbol outputs"
  lnc_translation_table$Original <- rownames(lnc_translation_table)
  lnc_translation_table <- lnc_translation_table[,c(2,1)]
  rownames(lnc_translation_table) <- seq_len(nrow(lnc_translation_table))
  
  lnc_coverage <- (length(names(all_custom_genes))/nrow(lnc_translation_table)) * 100
  
  save(result, all_genes, all_custom_genes, lnc_translation_table, lnc_coverage, lnc_symbol_coverage, file = paste0(analysis_out, "/lnc_analysis-", expset, "-", onto, ".rda"), compress = "xz", compression_level = "9")
}
