# Readme for lnc_analysis

For the long non-coding RNA analysis, both prostate and colorectal cancer KEGG genesets are analyzed together with two unique genesets for lncRNA's.

This analysis is performed in multiple steps, with the first being the annotation predictions on the lncRNA:

- Annotation prediction of lncRNA's functions. (done with the benchmarking pipeline)
- TopoICSim Intraset similarity calculations between the original KEGG geneset with annotation predicted lncRNAs as custom "genes".
- Clustering analysis and plotting of subsequent results (different annotation groups).

The last two of these steps are performed by `topo_lnc.R` and `clustering.R` respectively.

`method_coverage.R` plots the coverage percentages of correlation methods.