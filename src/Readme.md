# README FOR CODE

To understand the structure of the project, also consider reading the [main Readme](../Readme.md) and the [R_code Readme](snakemake_pipeline/scripts/R_scripts/Readme.md) and [Snakemake Readme](snakemake_pipeline/Readme.md) for the pipeline.

## `data_n_prep`

Code files for data preperation that are shared between analyses

## `lnc_analysis`

Code files for cancer related lncRNA analyses

## `reliability_measure`

Code files related to the reliability measure for predicting GO term annotation prediction quality.

## `snakemake_pipeline`

The snakemake pipeline runs a rigorous topoicsim "re-annotation" benchmark by considering multiple gene- and expressionsets.
The input files can be found in the [Bitbucket download page](https://bitbucket.org/Berghopper/lncrna_thesis_prj/downloads/) in the `ATTACHEMENTS.zip`.
The pipeline also tries to evaluate topoicsim values on selected genes.
