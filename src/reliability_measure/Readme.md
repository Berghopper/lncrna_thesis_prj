# Readme for reliability measure

This directory contains scripts to parse data for the reliability measure on lncRNA annotation predictions.

Because lncRNA's can't directly be compared to their "real" annotation (because they lack one), a reliability measure needs to made.
This reliability measure can be based on the TopoICSim similarity between real genes and their annotation predicted counterpart.
The snakemake pipeline in this repository is used for this purpose.

Scripts:

- `go_geneset.R` - outputs three custom genesets based on IC (see main readme or report).
- `pipeline_output_aggregation.R` - Used for combining all pipeline outputs.