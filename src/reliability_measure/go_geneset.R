# BASAL INPUTS (args)
# 1 - out_dir (output folder for geneset files)

library(GAPGOM)
library(data.table)
library(Cairo)

args <- commandArgs(trailingOnly = TRUE)

# ARGS

out_dir <- args[1]
dir.create(file.path(out_dir), showWarnings = FALSE)

# query GO annotations
go_data <- set_go_data("human", "MF", computeIC = TRUE, keytype= "SYMBOL")
gene_anno <- go_data@geneAnno
gene_anno_t <- data.table(gene_anno)

# get the frequency of each GO
gene_annotation_gofreqs <- gene_anno_t[, .N, by=SYMBOL]

# generate histogram of go frequency

Cairo(file=paste0(out_dir, "/hist_gofreq.png"))
hist(gene_annotation_gofreqs$N, breaks = max(gene_annotation_gofreqs$N), col = "#34cbcb", main = "gene frequency per x amount of GO terms. (histogram)", xlab = "Amount of GO terms", ylab = "gene frequency")
dev.off()

# geneset selector (at least freq of 2 and less than 10)
geneset <- gene_annotation_gofreqs[gene_annotation_gofreqs$N > 1,]
geneset <- geneset[geneset$N < 10,]
geneset$IC <- 0

# calculate the mean Information content over each gene
for (gene in geneset$SYMBOL) {
  gene_gos <- gene_anno_t[gene_anno_t$SYMBOL==gene,]$GO
  geneset[geneset$SYMBOL==gene, ]$IC <- mean(go_data@IC[gene_gos])
}

# remove all infinites, na's and order the df
is.infinite.data.frame <- function(x)
  do.call(cbind, lapply(x, is.infinite))
geneset <- geneset[!is.na(geneset$IC),]
geneset <- geneset[!is.infinite(geneset$IC),]
geneset <- geneset[order(geneset$IC, decreasing = T),]
rownames(geneset) <- seq_len(nrow(geneset))

# generate genesets and save  
geneset_middle_lowerhalf <- geneset[geneset$IC < mean(geneset$IC),][1:75,]
geneset_middle_upperhalf <- geneset[geneset$IC > mean(geneset$IC),]
geneset_middle_upperhalf <- geneset_middle_upperhalf[
  order(geneset_middle_upperhalf$IC, decreasing = FALSE)] # sort geneset in reverse
geneset_middle_upperhalf <- geneset_middle_upperhalf[1:75,]
geneset_middle <- rbind(geneset_middle_lowerhalf, geneset_middle_upperhalf)

write.table(geneset_middle$SYMBOL, file=paste0(out_dir, "/custom_geneset_avg.txt"), quote = FALSE, row.names=FALSE, col.names = FALSE)

geneset_topic <- geneset[1:150,]
write.table(geneset_topic$SYMBOL, file=paste0(out_dir, "/custom_geneset_high.txt"), quote = FALSE, row.names=FALSE, col.names = FALSE)

geneset_lowic <- geneset[order(geneset$IC, decreasing = F),]
geneset_lowic <- geneset_lowic[1:150,]
write.table(geneset_lowic$SYMBOL, file=paste0(out_dir, "/custom_geneset_low.txt"), quote = FALSE, row.names=FALSE, col.names = FALSE)

