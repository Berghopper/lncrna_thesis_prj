#!/usr/bin/env bash

echo $1
echo $2
echo $3
sbatch -A nn3556k --time=24:00:00 --nodes=1 $3 --cpus-per-task=$1 --mem-per-cpu=$2
