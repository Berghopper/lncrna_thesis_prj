#!/usr/bin/env python3
"""
Script that accepts multiple bash scripts and multithreads them.
"""
__author__ = "Casper Peters"
__credits__ = ["Casper Peters"]
__version__ = "0.1"
__maintainer__ = "Casper Peters"
__status__ = "Production"
# imports
import sys
import os
import time
import multiprocessing as mp
import traceback

# constants
# classes
# functions

def do_bash_command(bash_command):
    return_code = os.system(bash_command)
    if return_code != 0:
        sys.exit(return_code)
    return(0)

def check_loop(processes, ncores):
    jobs_left = len(processes)
    job_states = [0]*len(processes)
    jobs_running = 0
    while jobs_left != 0:
        #print(jobs_running)
        #print(jobs_left)
        for i, proc in enumerate(processes):
            if job_states[i] == 0:
                if jobs_running < ncores:
                    #print("core not utilized!")
                    proc.start()
                    #print("starting job")
                    jobs_running += 1
                    job_states[i] = 1
                else:
                    #print("job queued")
                    continue
            if job_states[i] == 1:
                #print("currently running")
                if not proc.is_alive():
                    jobs_left -= 1 
                    jobs_running -= 1
                    job_states[i] = 2
        time.sleep(0.01)
                

def main(argv=None):
    """
    initiates program
    """
    if argv is None:
        argv = sys.argv
    # work
    ncores = int(argv[1])
    ncommands_file = argv[2]
    with open(ncommands_file) as command_file:
        rawlines = command_file.readlines()
    ncommands = [line.replace("\n","") for line in rawlines]
    #print(ncores)
    #print(ncommands)
    processes = []
    for bash_command in ncommands:
        #print(bash_command)
        a_process = mp.Process(target=do_bash_command, args=[bash_command])
        processes.append(a_process)
    check_loop(processes, ncores)
    return 0


# initiate
if __name__ == "__main__":
    sys.exit(main())
