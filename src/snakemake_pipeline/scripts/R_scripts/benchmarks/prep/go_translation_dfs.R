# prepares go data given the organism and ontology

#### INPUTS
# 1 - organism
# 2 - ontology
# 3 - expset
# 4 - go_data
# 5 - output file
####

print("go_translation_dfs.R started")

# load libs (and custom functions)
suppressWarnings(suppressMessages(library(GAPGOM)))

## set stringsasfactors to false to ensure data is loaded properly.
options(stringsAsFactors = FALSE, digits = 22)
data.table::setDTthreads(1)

# GLOBAL VARIABLES

args <- commandArgs(trailingOnly = TRUE)

organism <- args[1]
ontology <- args[2]
load(args[3]) # loads expset and other variables (keytype and other expression data)
load(args[4]) # all_go_datas 
outfile <- args[5]

# organism <- "human"
# ontology <- "MF"
# load("/home/casper/OUTPUTS/prepped_datasets/expsets/expression_table_GSEA_Sboner.rda")
# load("/home/casper/OUTPUTS/prepped_datasets/godata/human-MF.rda")
# outfile <- "~/test.rda"

go_data <- all_go_datas[[keytype]]
translation_df <- GAPGOM:::.generate_translation_df(expset, organism, ontology, keytype, 
                                                        go_data=go_data)
# RDA
save(translation_df, 
     file = outfile,
     compress = "xz", compression_level = 9
)