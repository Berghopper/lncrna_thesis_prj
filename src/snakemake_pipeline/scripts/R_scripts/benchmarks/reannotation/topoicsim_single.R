# this script runs a single topoicsim instance to calculate the difference
# between a re-annotation and original annotation of a gene.
#### INPUTS
# 1 - gene id
# 2 - input lncRNA2GOA result file (annotation file)
# 3 - significance cutoff for GO terms you want to use
# 4 - organism
# 5 - ontology
# 6 - topo output file
# 7 - topo output file of GO intraset similarity
# 8 - expression set file (required for keytype)
# 9 - go_data
####

# load libs (and custom functions)
suppressWarnings(suppressMessages(library(GAPGOM)))
suppressWarnings(suppressMessages(library(tools)))

## set stringsasfactors to false to ensure data is loaded properly.
options(stringsAsFactors = FALSE, digits = 22)
data.table::setDTthreads(1)

# GLOBAL VARIABLES

args <- commandArgs(trailingOnly = TRUE)

gene_id <- args[1]
annotation_file <- args[2]
annotation_significance <- as.numeric(args[3])
organism <- args[4]
ontology <- args[5]
output_file <- args[6]
output_file_go <- args[7]
expset_file <- args[8]
load(expset_file) # load for keytype
# remove expset, and garbage collect.
rm(expset)
gc()
load(args[9]) # godata
go_data <- all_go_datas[[keytype]]

# load annotation
annotation_table <- as.data.frame(read.table(annotation_file))
# set filtered output (cut on significance)
annotation_file_filtered <- file_path_sans_ext(annotation_file)


# store empty result if annotation is empty, continue like normal if not.
if (is.data.frame(annotation_table) && !any(is.na(annotation_table[1,]))) {
  # cut on significance and save this output back to the annotation directory
  annotation_table <- annotation_table[(as.numeric(
    annotation_table[, 4]) < annotation_significance), ]
  write.table(annotation_table, 
              annotation_file_filtered,
              sep = " ", 
              dec = "."
  )
  custom_go_annotation <- annotation_table[["GOID"]]
  custom <- list(custom=custom_go_annotation)
  result <- GAPGOM::topo_ic_sim_genes(organism, ontology, c(), gene_id, 
                                      custom_genes1 = custom,
                                      idtype = keytype,
                                      go_data = go_data, 
                                      progress_bar = FALSE)
  save(result,
       file = output_file,
       compress = "xz", compression_level = 9
  )
  go_list <- as.list(annotation_table$GOID)
  names(go_list) <- annotation_table$GOID
  result_go <- GAPGOM::topo_ic_sim_genes(organism, ontology, c(), c(), 
                            custom_genes1 = go_list, 
                            custom_genes2 = go_list, 
                            idtype = keytype, 
                            go_data = go_data, 
                            progress_bar = FALSE)
  save(result_go,
       file = output_file_go,
       compress = "xz", compression_level = 9
  )
} else {
  # save empty result, nothing to do.
  write.table(as.data.frame(""), # export empty df
              annotation_file_filtered,
              sep = " ", 
              dec = "."
  )
  result <- list(GeneSim = NULL, AllGoPairs = NULL)
  save(result,
       file = output_file,
       compress = "xz", compression_level = 9
  )
  save(result,
       file = output_file_go,
       compress = "xz", compression_level = 9
  )
}
