# README FOR R CODE

The scripts in this folder are small Rscripts that are used by snakemake to run benchmarks with GAPGOM.

Scripts are listed below per benchmark type for GAPGOM.

All listed files should be ran in the order(s) listed below for the pipeline.

### Preperation phase
- `benchmarks/prep/colnc_prep.R`/`benchmarks/prep/morten_prep.R` prepares expressionset objects for both morten's and colnc data
- `benchmarks/prep/calc_var_gensets.R` - calculates top variant genes from genesets
- `benchmarks/prep/prep_ontos.R` prepares `go_data` for each ontology (MF, CC, BP)
- `benchmarks/prep/go_translation_dfs.R` prepares translation dataframes for each generated `go_data`, these are faster to use because they are regular dfs. Normally they are generated internally, but by doing it earlier, we are preventing unnecesary calculations.

## TopoICSim similarity scoring with "re-annotation" of genes

### Execution phase
- `benchmarks/reannotation/lncRNA2GOA.R` - predicts annotation for a single gene and expressionset.
- `benchmarks/reannotation/topoicsim_single.R` - should score each gene on it's old vs new annotation.

### combining and plotting phase
- `benchmarks/reannotation/topoicsim_combine.R` - should combine the results for each dataset combination and plot output for said combination.
- `benchmarks/reannotation/plot_average.R` - plots averaged results (for each ontology).

## TopoICSim geneset analysis
- `benchmarks/geneset_intrasetsim/topoicsim_gentest.R` Generates topoicsims based on input genes and annotation prediction outputs.