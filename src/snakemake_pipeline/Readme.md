## Snakemake files

The snakemake pipeline (in `snakemake_pipeline`) contains the following files:

- `start.snakefile` - The MAIN snakefile, this is the starting point.
- `config.yaml` The pipeline configuration file.
- `includes/glob_funs.snakefile` Contains different python functions to parse files and file contents.
- `includes/global_vars.snakefile` File that sets all global variables that are used elsewhere in the pipeline workflow.
- `includes/prep/misc.snakefile` Contains miscellaneous preperation rules.
- `includes/benchmarks/prep/expset_prep.snakefile` Expression set preperation.
- `includes/benchmarks/prep/geneset_prep.snakefile` Geneset preperation.
- `includes/benchmarks/reannotation/lncrna2goa.snakefile` lncrna2goa reannotation rules.
- `includes/benchmarks/reannotation/topoicsim.snakefile` topoicsim reannotation rules.
- `includes/benchmarks/reannotation/plots.snakefile` output combining and plotting for reannotation.
- `includes/benchmarks/geneset_intrasetsim/topoicsim.snakefile` topoicsim geneset analysis rules.
- `scripts/R_scripts` folder containing R scripts, see [Readme](snakemake_pipeline/scripts/R_scripts/Readme.md).

With the `start.snakefile` there's a few rules available for running/benchmarking:

- `all` - The default rule, runs ALL processes.
- `annotation_predict` - Run only the annotation step - skips TopoICSim comparisons.
- `prep_only_ambiguous` - Prepares files that don't change in different config settings.
- `prep_only` - Only prepares datasets, but does not run analyses.
- `clean` - Cleans main outputs only.
- `clean_all` - Cleans whole output directory.
- `clean_topo` - Clean topoicsim specific outputs only.

For the snakemake input, 5 different directories can be specified (in the config):

- `geneset_dir` - genesets with HALLMARK header.
- `colncrna_expsets` - expressionsets from the colncrna repository (GSE-datasets)
- `morten_geneset_dir` - genesets without any header
- `morten_expsets` - expressionsets that are transposed and contain only SYMBOL ids
- `prepared_expsets` - R objects that are ready to be loaded directly into the pipeline without any further preparation (FANTOM, TCGA)

## Slurm

The `slurm` folder contains two files:

- `slurm_eating_snakemake.sh` This is the slurm setup script used for the snakemake benchmark pipeline.
- `config.yaml` This is the pipeline config, but tweaked for the slurm cluster