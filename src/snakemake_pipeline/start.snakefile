# main snakefile to start other processes

# IMPORTS
import os
import multiprocessing # NOT USED?
import pandas
import csv
import numpy as np
from itertools import product

# Load in config files
configfile: "config.yaml"

# load in globals
include: "includes/glob_funcs.snakefile" # pythonic functions
include: "includes/global_vars.snakefile" # sets global variables
# Load in all includes
# benchmark preperation
include: "includes/benchmarks/prep/expset_prep.snakefile"
include: "includes/benchmarks/prep/geneset_prep.snakefile"
include: "includes/benchmarks/prep/misc.snakefile"
# reannotation
include: "includes/benchmarks/reannotation/lncrna2goa.snakefile"
include: "includes/benchmarks/reannotation/topoicsim.snakefile"
include: "includes/benchmarks/reannotation/plots.snakefile"
# geneset analysis
include: "includes/benchmarks/geneset_intrasetsim/topoicsim.snakefile"

# rule all to get benchmarks (might be expanded later).
rule all:
    input:
        config["control_files_dir"]+"BENCHMARKS_DONE.txt"
    output:
        config["main_output_dir"]+"output.zip"
    params:
        cpu = "1",
        mem = "1G"
    shell:
        # create zip after everything is done.
        "cd "+config["main_output_dir"]+" && zip -r9 {output} ./*"

# rule to only get benchmarks
rule benchmarks:
    input:
        expand(config["main_output_dir"]+PLOTS_OUT+"{ontology}.png", ontology=ONTOLOGIES),
        # expand for calculating intraset similarities of analyzed genesets. (uncomment to activate)
        #expand(config["main_output_dir"]+TOPO_GSET_OUT+"{expset}-{geneset}-{organism}-{ontology}.rda", expset=EXPSETS, geneset=GENESETS, organism=ORGANISMS, ontology=ONTOLOGIES)
    output:
        touch(config["control_files_dir"]+"BENCHMARKS_DONE.txt")
    params:
        cpu = "1",
        mem = "1G"

# rule to only annotate (useful for non-benchmark runs)
rule annotation_predict:
    input:
        config["main_output_dir"]+TOPO_OUT, #OUTDIRS
        config["control_files_dir"]+TOPO_OUT+"commands/",
        expand(config["control_files_dir"]+ANNOTATION_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.txt", 
            expset=EXPSETS, geneset=GENESETS, organism=ORGANISMS, 
            ontology=ONTOLOGIES),
        godat = expand(config["main_output_dir"]+PREPPED_GODATS+\
            "{organism}-{ontology}.rda", organism=ORGANISMS, 
            ontology=ONTOLOGIES),
        geneset = expand(config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset}.txt", expset=EXPSETS, geneset=GENESETS),
        expset = expand(config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda", expset=EXPSETS)
    params:
        cpu = "1",
        mem = "1G"

# rule to only get the prepared datasets that don't change per config
# setting.
rule prep_only_ambiguous:
    input:
        expand(config["main_output_dir"]+PREPPED_EXPSETS+
            "{expset}.rda", expset=EXPSETS),
        godat = expand(config["main_output_dir"]+PREPPED_GODATS+
            "{organism}-{ontology}.rda", organism=ORGANISMS, ontology=ONTOLOGIES),
        go_translation_df = expand(config["main_output_dir"]+\
            PREPPED_GO_TRANSLATIONS+\
            "{expset}-{organism}-{ontology}.rda", expset=EXPSETS,
            organism=ORGANISMS, ontology=ONTOLOGIES)
    params:
        cpu = "1",
        mem = "1G"

# rule to only get the prepared datasets.
rule prep_only:
    input:
        expand(config["main_output_dir"]+PREPPED_EXPSETS+
            "{expset}.rda", expset=EXPSETS),
        geneset = expand(config["main_output_dir"]+PREPPED_GENESETS+
            "{expset}-{geneset}.txt", expset=EXPSETS, geneset=GENESETS),
        godat = expand(config["main_output_dir"]+PREPPED_GODATS+
            "{organism}-{ontology}.rda", organism=ORGANISMS, ontology=ONTOLOGIES),
        go_translation_df = expand(config["main_output_dir"]+\
            PREPPED_GO_TRANSLATIONS+\
            "{expset}-{organism}-{ontology}.rda", expset=EXPSETS,
            organism=ORGANISMS, ontology=ONTOLOGIES)
    params:
        cpu = "1",
        mem = "1G"   
        
# rule to reset everything of the pipeline
rule clean_all:
    params:
        cpu = "1",
        mem = "1G"
    shell:
        "rm -rf "+config["main_output_dir"]

# only delete main outputs and keep prepped datasets (takes long and it's just a waste otherwise)
rule clean:
    params:
        cpu = "1",
        mem = "1G"
    run:
        shell("rm -rf "+config["main_output_dir"]+ANNOTATION_OUT)
        shell("rm -rf "+config["main_output_dir"]+TOPO_OUT)
        shell("rm -rf "+config["main_output_dir"]+TOPO_GSET_OUT)
        shell("rm -rf "+config["main_output_dir"]+PLOTS_OUT)
        shell("rm -rf "+config["control_files_dir"])
        shell("rm -rf "+config["main_output_dir"]+"output.zip")
        shell("rm -rf "+config["main_output_dir"]+"unique_genes_ensembl.txt")
        shell("rm -rf "+config["main_output_dir"]+"unique_genes_symbol.txt")

# cleans topoicsim outputs
rule clean_topo:
    params:
        cpu = "1",
        mem = "1G"
    run:
        shell("rm -rf "+config["main_output_dir"]+ANNOTATION_OUT+"*.txt")
        shell("rm -rf "+config["control_files_dir"]+"BENCHMARKS_DONE.txt")
        shell("rm -rf "+config["main_output_dir"]+TOPO_GSET_OUT+"/*")
        shell("rm -rf "+config["main_output_dir"]+TOPO_OUT+"/*")
        shell("rm -rf "+config["control_files_dir"]+TOPO_OUT+"/*.txt")
        shell("rm -rf "+config["main_output_dir"]+PLOTS_OUT+"/*")
        shell("rm -rf "+config["main_output_dir"]+"output.zip")
