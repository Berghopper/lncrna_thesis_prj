#NEW RULES
rule topoicsim_all_genesets:
    input:
        godat = config["main_output_dir"]+PREPPED_GODATS+\
            "{organism}-{ontology}.rda",
        geneset = config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset}.txt",
        expset = config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda"
    output:
        config["main_output_dir"]+\
            TOPO_GSET_OUT+"{expset}-{geneset}-{organism}-{ontology}.rda"
    params:
        rscript = R_GENTEST_TOPO,
        cpu = "1",
        mem = "16G"
    run:
        shell(params.rscript+
            list_to_params([wildcards.organism, 
                wildcards.ontology, 
                output[0], 
                input.expset, 
                input.godat,
                input.geneset]))
