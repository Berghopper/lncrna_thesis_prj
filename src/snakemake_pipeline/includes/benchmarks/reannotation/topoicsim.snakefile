# topoicsim to define re-annotation scores

rule topoicsim:
    input:
        config["main_output_dir"]+TOPO_OUT, #OUTDIRS
        config["control_files_dir"]+TOPO_OUT+"commands/",
        config["control_files_dir"]+ANNOTATION_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.txt",
        godat = config["main_output_dir"]+PREPPED_GODATS+\
            "{organism}-{ontology}.rda",
        geneset = config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset}.txt",
        expset = config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda"
    output:
        touch(config["control_files_dir"]+TOPO_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.txt")
    params:
        rscript = R_RE_ANNO_TOPO,
        pyscript = PY_MULTITHREAD_SCRIPT,
        big_job_nclust = config["big_job_cores"],
        annotation_out_prefix = config["main_output_dir"]+\
            ANNOTATION_OUT+\
            "{expset}-{geneset}-"+\
            "{organism}-{ontology}-",
        outfile_prefix = config["main_output_dir"]+TOPO_OUT+\
                    "{expset}-{geneset}-"+\
                    "{organism}-{ontology}-",
        commands_out_file = config["control_files_dir"]+TOPO_OUT+\
            "commands/{expset}-{geneset}-{organism}-{ontology}.txt",
        lncrna2goa_significance = config["lncrna2goa_significance"],
        cpu = config["big_job_cores"],
        mem = "4G"
    threads: int(config["big_job_cores"])
    run:
        # run function to get amount of genes everytime.
        amount_genes = get_gene_amount(input.geneset)
        bash_commands = list()
        for i in range(1, amount_genes):
            gene_id = translate_gene_no_to_id(input.geneset, i)
            bash_commands.append(params.rscript+\
                list_to_params([
                    gene_id,
                    params.annotation_out_prefix+str(gene_id)+".txt.RAW",
                    params.lncrna2goa_significance,
                    wildcards.organism,
                    wildcards.ontology,
                    params.outfile_prefix+str(gene_id)+".rda",
                    params.outfile_prefix+str(gene_id)+"_GOSIM.rdata",
                    input.expset,
                    input.godat
                ])
            )
        write_list_to_file(bash_commands, params.commands_out_file)
        shell(params.pyscript+list_to_params([params.big_job_nclust, 
            params.commands_out_file]))
