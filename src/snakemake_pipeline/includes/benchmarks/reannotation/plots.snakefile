# make average plots for easily understandable visual output 
# (per ontology).
rule final_plots:
    input:
        expand(config["main_output_dir"]+PLOTS_OUT+\
            "{expset}-{geneset}-{organism}-{{ontology}}.txt",
            expset=EXPSETS, geneset=GENESETS, organism=ORGANISMS
            )
    output:
        config["main_output_dir"]+PLOTS_OUT+"{ontology}.png"
    params:
        rscript = R_RE_SUM_PLOTS,
        plots_out = config["main_output_dir"]+PLOTS_OUT+\
            "*{ontology}*.txt",
        plot_name = "TopoICSim score for {ontology}",
        cpu = "1",
        mem = "2G"
    shell:
        "{params.rscript}"+\
            list_to_params([
                "{output}",
                "{params.plot_name}",
                "{params.plots_out}"
                ])

# rule to combine topoicsim reannotation output and generate plots for 
# each sample
rule topo_combine_and_generate_plots:
    input:
        config["control_files_dir"]+TOPO_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.txt"
    output:
        plot_out = config["main_output_dir"]+PLOTS_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.png",
        summary_out = config["main_output_dir"]+PLOTS_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.txt"
    params:
        rscript = R_RE_PLOTS,
        topo_outs = config["main_output_dir"]+TOPO_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}-*.rda",
        plot_name = "TopoICSim score for {expset}, {geneset}, "+\
            "{organism}, {ontology}",
        cpu = "1",
        mem = "4G"
    shell:
        "{params.rscript}"+\
            list_to_params([
                "{output.plot_out}",
                "{params.plot_name}",
                "{output.summary_out}",
                "{params.topo_outs}"
                ])
