# lncrna2goa for benchmarking.

rule lncrna2goa:
    input:
        config["main_output_dir"]+ANNOTATION_OUT, # outdirs
        config["control_files_dir"]+ANNOTATION_OUT+"commands/",
        expset = config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda",
        geneset = config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset}.txt",
        go_translation_df = config["main_output_dir"]+\
            PREPPED_GO_TRANSLATIONS+"{expset}-{organism}-{ontology}.rda"
    output:
        touch(config["control_files_dir"]+ANNOTATION_OUT+\
            "{expset}-{geneset}-{organism}-{ontology}.txt")
    params:
        rscript = R_RE_ANNO_LNC,
        pyscript = PY_MULTITHREAD_SCRIPT,
        big_job_nclust = config["big_job_cores"],
        outfile_prefix = config["main_output_dir"]+ANNOTATION_OUT+\
            "{expset}-{geneset}-"+\
            "{organism}-{ontology}-",
        commands_out_file = config["control_files_dir"]+ANNOTATION_OUT+\
            "commands/{expset}-{geneset}-{organism}-{ontology}.txt",
        lncrna2goa_enrichment_cutoff = config["lncrna2goa_enrichment_cutoff"],
        cpu = config["big_job_cores"],
        mem = "4G"
    threads: int(config["big_job_cores"])
    run:
        # run function to get amount of genes everytime.
        amount_genes = get_gene_amount(input.geneset)
        bash_commands = list()
        for i in range(1, amount_genes):
            gene_id = translate_gene_no_to_id(input.geneset, i)
            bash_commands.append(params.rscript+\
                list_to_params([
                    input.expset,
                    input.geneset,
                    gene_id,
                    wildcards.organism,
                    wildcards.ontology,
                    params.lncrna2goa_enrichment_cutoff,
                    params.outfile_prefix+str(gene_id)+".txt.RAW",
                    input.go_translation_df,
                    config["include_anno_only"]
                ])
            )
        write_list_to_file(bash_commands, params.commands_out_file)
        shell(params.pyscript+list_to_params([params.big_job_nclust, 
            params.commands_out_file]))
