# miscellaneous preperation snake rules (onto preps)

# prepare directories, only used if necessary for input/output (normally snakemake handles this).
rule prep_misc_dirs:
    output:
        directory(config["main_output_dir"]+ANNOTATION_OUT),
        directory(config["main_output_dir"]+TOPO_OUT),
        directory(config["control_files_dir"]+ANNOTATION_OUT+\
            "commands/"),
        directory(config["control_files_dir"]+TOPO_OUT+"commands/")
    params:
        cpu = "1",
        mem = "1G"
    run:
        for directory in output:
            make_directory(directory)

# prepare go_data for all ontologies (decreases requests)
rule prep_ontos:
    output:
        config["main_output_dir"]+PREPPED_GODATS+\
            "{organism}-{ontology}.rda"
    params:
        rscript = R_PREP_ONTOLOGIES,
        out_dir = config["main_output_dir"]+PREPPED_GODATS,
        cpu = "1",
        mem = "8G"
    shell:
        "{params.rscript}"+
            list_to_params([
                "{wildcards.organism}",
                "{wildcards.ontology}",
                "{params.out_dir}"]+
                KEYTYPES)

# prepare go_data translation tables for all expsets
rule prep_translation_dfs:
    input:
        expset = config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda",
        go_data = config["main_output_dir"]+PREPPED_GODATS+\
            "{organism}-{ontology}.rda"
    output:
        outfile = config["main_output_dir"]+PREPPED_GO_TRANSLATIONS+\
            "{expset}-{organism}-{ontology}.rda"
    params:
        rscript = R_PREP_TRANSLATION_DF,
        cpu = "1",
        mem = "8G"
    shell:
        "{params.rscript}"+
            list_to_params(["{wildcards.organism}",
                "{wildcards.ontology}",
                "{input.expset}",
                "{input.go_data}",
                "{output.outfile}"])
