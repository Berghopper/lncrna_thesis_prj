# rules for prepping expression sets

# merge expressionset rules for wildcard generalization.
rule prep_expsets_merge:
    input:
        expand(config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset_colnc}.rda", expset_colnc = EXPSETS_COLNCRNA),
        expand(config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset_morten}.rda", expset_morten = EXPSETS_MORTEN),
        expand(config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset_prepped}.rda", expset_prepped = EXPSETS_RDAS)
    params:
        cpu = "1",
        mem = "1G"
    output:
        config["main_output_dir"]+PREPPED_EXPSETS+"{expset}.rda"

# prepare colnc expressionsets.
rule prep_expsets_colnc:
    input:
        config["colncrna_expsets"]+"{expset_colnc}.txt"
    output:
        config["main_output_dir"]+PREPPED_EXPSETS+"{expset_colnc}.rda"
    params:
        rscript = R_PREP_EXPSET_COLNC,
        output_dir = config["main_output_dir"]+PREPPED_EXPSETS,
        cpu = "1",
        mem = "4G"
    shell:
        "{params.rscript}"+\
            list_to_params([
                "{input}",
                "{params.output_dir}",
                config["keytype_colncrna"]
                ])

# prepare morten's expressionsets (uses SYMBOL keytype instead)
rule prep_expsets_morten:
    input:
        config["morten_expsets"]+"{expset_morten}.txt"
    output:
        config["main_output_dir"]+PREPPED_EXPSETS+"{expset_morten}.rda"
    params:
        rscript = R_PREP_EXPSET_MORTEN,
        output_dir = config["main_output_dir"]+PREPPED_EXPSETS,
        cpu = "1",
        mem = "4G"
    shell:
        "{params.rscript}"+\
            list_to_params([
                "{input}",
                "{params.output_dir}",
                config["keytype_morten"]
                ])

rule prep_expsets_prepared:
    input:
        config["prepared_expsets"]+"{expset_prepped}.rda"
    output:
        config["main_output_dir"]+PREPPED_EXPSETS+"{expset_prepped}.rda"
    params:
        cpu = "1",
        mem = "1G"
    shell:
        "cp {input} {output}"
