# rules for prepping genesets (depends on expset)

# merge just like in expset_prep, expset is a depenency and is a
# wildcard defined in all, that's why it's escaped with more curly
# brackets.
rule prep_genesets_merge:
    input:
        expand(config["main_output_dir"]+PREPPED_GENESETS+\
            "{{expset}}-{geneset_msigdb}.txt",
            geneset_msigdb = GENESETS_MSIGDB
            ),
        expand(config["main_output_dir"]+PREPPED_GENESETS+\
            "{{expset}}-{geneset_morten}.txt",
            geneset_morten = GENESETS_MORTEN
            )
    params:
        cpu = "1",
        mem = "1G"
    output:
        config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset}.txt"

# only difference between morten and msigdb geneset prep is the starting
# point of the file, msigdb has a header while mortens set does not.
rule prep_genesets_msigdb:
    input:
        expset = config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda",
        geneset = config["geneset_dir"]+"{geneset_msigdb}.txt"
    output:
        config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset_msigdb}.txt"
    params:
        rscript = R_PREP_CALC_VAR_GENESETS,
        out_dir = config["main_output_dir"]+PREPPED_GENESETS,
        start_genes = 3,
        cpu = "1",
        mem = "4G"
    shell:
        "{params.rscript}"+
            list_to_params([
                "{input.expset}",
                "{input.geneset}",
                config["geneset_cutoff"],
                "{params.out_dir}",
                "{params.start_genes}",
                config["variance_colnc"],
                config["correct_symbol"]
                ])

rule prep_genesets_morten:
    input:
        expset = config["main_output_dir"]+PREPPED_EXPSETS+\
            "{expset}.rda",
        geneset = config["morten_geneset_dir"]+"{geneset_morten}.txt"
    output:
        config["main_output_dir"]+PREPPED_GENESETS+\
            "{expset}-{geneset_morten}.txt"
    params:
        rscript = R_PREP_CALC_VAR_GENESETS,
        out_dir = config["main_output_dir"]+PREPPED_GENESETS,
        start_genes = 1,
        cpu = "1",
        mem = "4G"
    shell:
        "{params.rscript}"+
            list_to_params([
                "{input.expset}",
                "{input.geneset}",
                config["geneset_cutoff"],
                "{params.out_dir}",
                "{params.start_genes}",
                config["variance_morten"],
                config["correct_symbol"]
                ])
