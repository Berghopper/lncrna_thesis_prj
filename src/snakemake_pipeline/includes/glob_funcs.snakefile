# Global functions

def get_files_from_dir(directory, extension, sans_anything = False):
    """
    read files to list from dir because I don't want to have to specify
    EVERY intermediary input/output file...
    
    sans_anything --> without path and exentension
    """
    files = []
    for i in os.listdir(directory):
        if (directory+i).endswith(extension):
            if sans_anything:
                files.append(os.path.splitext(i)[0])
            else:
                files.append(directory+i)
    return files

def get_gene_amount(geneset_file):
    """
    get the amount of genes in a prepared geneset_file 
    (useful for lncrna2goa and topoicsim rules)
    """
    return sum(1 for line in open(geneset_file))

def translate_gene_no_to_id(geneset_file, gene_no):
    """
    Translates positional index of gene in geneset file to its 
    corresponding ID
    """
    df = pandas.read_csv(geneset_file, sep=" ")
    return(df.query(str(gene_no))[0])

def list_to_params(listy, subquoted = False):
    """
    Adds quotation marks for shell environment so each parameter is 
    neatly isolated.
    subquoted means that there's already commands in the params.
    """
    if subquoted:
        newlisty = list()
        for param in listy:
            newlisty.append(param.replace("\"", "\\\""))
        return " \""+"\" \"".join(newlisty)+"\""
    return " \""+"\" \"".join(listy)+"\""

def filter_combinator(combinator, whitelist):
    """
    Creates alternate combinator function for expand, such that a
    whitelist can be used.
    
    Based on:
    https://stackoverflow.com/questions/41185567/how-to-use-expand-in-snakemake-when-some-particular-combinations-of-wildcards-ar
    """
    def filtered_combinator(*args, **kwargs):
        for wc_comb in combinator(*args, **kwargs):
            # Use frozenset instead of tuple
            # in order to accomodate
            # unpredictable wildcard order
            if frozenset(wc_comb) in whitelist:
                yield wc_comb
    return filtered_combinator

def create_whitelist_genes(wildcards):
    """
    Creates whitelist for all wildcard combinations possible.
    Which genes are used with which wildcard matters because not all
    genes reside in all expsets.
    """
    # get wildcards from checkpoint output
    all_genes_out = checkpoints.split_up_genes_each_sample_lnc.get(
        **wildcards).output[0]
    gene_wildcards = glob_wildcards(os.path.join(all_genes_out, 
        "{gene}.txt")).gene
    whitelist = [{("expset", wildcards.expset),
                  ("geneset", wildcards.geneset),
                  ("ontology", wildcards.ontology), 
                  ("organism", wildcards.organism), 
                  ("gene", gene)
                 } for gene in gene_wildcards]
    return whitelist

def get_genes_for_sample(geneset_file):
    """
    Gets all genes present in a given geneset sample (only grabs
    uniques).
    """
    return list(set(geneset_file_to_geneset_list(geneset_file)))

def pairwise_combine_uniques(geneset_file):
    """
    creates unique combinations of genes given a geneset file.
    """
    amount_genes = get_gene_amount(geneset_file)
    gene_pair_combos = list()
    for i in range(1, amount_genes):
        gene_id1 = translate_gene_no_to_id(geneset_file, i)
        for j in range(1, amount_genes):
            gene_id2 = translate_gene_no_to_id(geneset_file, j)
            if gene_id1 == gene_id2:
                continue
            elif gene_id1+"_"+gene_id2 in gene_pair_combos or \
                gene_id2+"_"+gene_id1 in gene_pair_combos:
                continue
            else:
                gene_pair_combos.append(gene_id1+"_"+gene_id2)
    return gene_pair_combos

def regex(v):
    """
    Create regular expression for 'v' value.
    This is for generating wildcard contraints.
    """
    return "(" + "|".join(v) + ")" 

def geneset_file_to_geneset_list(geneset_file):
    """
    Get the contents of a geneset file and convert all the values to a 
    list containing all genes
    """
    return pandas.read_csv(geneset_file, sep=" ")["x"].values.tolist()

def write_list_to_file(thelist, outfile):
    with open(outfile, "w+") as out_file:
        for item in thelist:
            out_file.write(str(item)+"\n")

def make_directory(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    else:
        if not os.path.isdir(directory):
            os.makedirs(directory)
