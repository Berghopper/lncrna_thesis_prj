# This file is for setting global variables in the snakemake pipeline
# environment.

# GLOBAL VARIABLES/WILDCARDS
EXPSETS_COLNCRNA = get_files_from_dir(config["colncrna_expsets"], 
    "txt", sans_anything=True)
GENESETS_MSIGDB = get_files_from_dir(config["geneset_dir"], "txt", 
    sans_anything=True)
EXPSETS_MORTEN = get_files_from_dir(config["morten_expsets"], "txt", 
    sans_anything=True)
GENESETS_MORTEN = get_files_from_dir(config["morten_geneset_dir"], 
    "txt", sans_anything=True)
## Already prepared expsets for special/big data (handy for non-straightforward cases)    
EXPSETS_RDAS = get_files_from_dir(config["prepared_expsets"], 
    "rda", sans_anything=True)
## Aggragations and hard-code unchangable known wildcards.
EXPSETS = EXPSETS_COLNCRNA+EXPSETS_MORTEN+EXPSETS_RDAS
GENESETS = GENESETS_MSIGDB+GENESETS_MORTEN
ORGANISMS = ["human"]
ONTOLOGIES = ["BP", "MF", "CC"]
KEYTYPES = [config["keytype_colncrna"], config["keytype_morten"]]
## output dirs
PREPPED_DATASETS = "prepped_datasets/"
PREPPED_EXPSETS = PREPPED_DATASETS+"expsets/"
PREPPED_GENESETS = PREPPED_DATASETS+"genesets/"
PREPPED_GODATS = PREPPED_DATASETS+"godata/"
PREPPED_GO_TRANSLATIONS = PREPPED_DATASETS+"go_translation_dfs/"
ANNOTATION_OUT = "custom_anno/"
TOPO_OUT = "topo_out/"
PLOTS_OUT = "plots_out/"
TOPO_GSET_OUT = "topo_geneset_only/"   

## R scripts
R_EXEC_BASE = config["r_dir"]+"Rscript "+config["rscript_dir"]
### prep
R_PREP_EXPSET_COLNC = R_EXEC_BASE+"benchmarks/prep/colnc_prep.R"
R_PREP_EXPSET_MORTEN = R_EXEC_BASE+"benchmarks/prep/morten_prep.R"
R_PREP_CALC_VAR_GENESETS = R_EXEC_BASE+\
    "benchmarks/prep/calc_var_gensets.R"
R_PREP_ONTOLOGIES = R_EXEC_BASE+"benchmarks/prep/prep_ontos.R"
R_PREP_TRANSLATION_DF = R_EXEC_BASE+"benchmarks/prep/go_translation_dfs.R"
### reannotation
R_RE_ANNO_LNC = R_EXEC_BASE+"benchmarks/reannotation/lncRNA2GOA.R"
R_RE_ANNO_TOPO = R_EXEC_BASE+\
    "benchmarks/reannotation/topoicsim_single.R"
R_RE_PLOTS = R_EXEC_BASE+"benchmarks/reannotation/topoicsim_combine.R"
R_RE_SUM_PLOTS = R_EXEC_BASE+"benchmarks/reannotation/plot_average.R"
### geneset analysis
R_GENTEST_TOPO = R_EXEC_BASE+\
    "benchmarks/geneset_intrasetsim/topoicsim_gentest.R"
R_GENTEST_TOPO_COMBINE = R_EXEC_BASE+\
    "benchmarks/geneset_intrasetsim/topoicsim_gentest_combine.R"

## python3 scripts
PY_EXEC_BASE = "python3 "+config["pyscript_dir"]
PY_MULTITHREAD_SCRIPT = PY_EXEC_BASE+"multi_process.py"

## misc
ENSEMBL_REGEXP = 'ENS[A-Z]+[0-9]{11}'

## Set global wildcard constraints (important)
wildcard_constraints:
    expset = regex(EXPSETS),
    geneset = regex(GENESETS),
    organism = regex(ORGANISMS),
    ontology = regex(ONTOLOGIES)
